function value_or_empty_string( variable ) {
  return (variable==undefined) ? "" : variable;
}

function cleaner_time( s ) { 
	var hours=Math.floor(s/3600) ; 
	var minutes=Math.floor(s/60)-(hours*60); 
	var seconds=Math.floor(s-(hours*3600)-(minutes*60) ); 
	var ret ='';
	if (hours>0) {
		if(hours<10) {
			ret+="0"+hours+":";
		} else {
			ret+=hours+":";
		}
	} else {
		ret+="00:";
	} 
	if (minutes>0) {
		if(minutes<10) {
			ret+="0"+minutes+":";
		} else {
			ret+=minutes+":";
		}
	} else {
		ret+="00:";
	} 
	if (seconds>0) {
		if(seconds<10) {
			ret+="0"+seconds;
		} else {
			ret+=seconds;
		}
	} else {
		ret+="00";
	} 
	return ret; 
}

function cleaner_filename( name ) {
  //remove the extension if it exists
  var ret = name.replace(/\.[A-z0-9]{2,4}$/,'');
  //replace . and _ with spaces
  ret = ret.replace(/[\._]/g,' ');
  return ret; 
}

function not_empty_value(test_value, default_value) {
	if(test_value == undefined || test_value == "") {
		return default_value;
	} else {
		return test_value;
	}
}

function rsplit(string, splitter) {
	var ret = ['',''];
	var bits = string.split(splitter);
	if ( bits.length > 1 ) {
		var base = bits.pop();
		ret[0] = bits.join(splitter);
		ret[1] = base;
	}
	return ret;
}
