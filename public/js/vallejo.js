/* Part of MuttonChop Mobile Web UI
 * copyright 2012 Jezra Lickter
 * Licensed GPLv3
 */
$(function(){	
	host = "localhost";
	port = 2876;
	//set some variables
	got_video = false;
	got_audio = false;
	got_casts = false;
	got_streams = false;
	current_page = undefined;
	current_selected_video = undefined;
	current_selected_audio	= undefined;
	current_selected_stream = undefined;
	current_selected_episode = undefined;
	current_selected_feed_id = undefined;
	current_volume = 0;
	check_downloads = false;
	updating_volume = false;
	player_timer_speed = 2000;
	download_status_speed = 2500;
	has_event_source = false;
	first_player_status = true;
	file_info_is_scrolling = false;
	stop_timer_get_player_status = false;
	stop_timer_get_download_status = false;
	
	//handle changing the visibility of the app
	visibility = 0;
	reading_data = true;
	
	//what is the last audio function?
	last_audio_view_function = null;
	//hide all of the pages
	$(".page").hide();
	//define some element vars based on id so we don't have to select over and over
	file_info = $("#file_info");
	//connect some the buttons we will need
	connect_buttons();
	//set up the player
	set_up_player();
	//set up the casts
	set_up_casts();
	//set up the streams 
	set_up_streams();
	//set up the swipe handling
	set_up_swipe();
	//handle things changing size
	set_up_resizes();
	//set up the centereds
	set_up_centereds();
	
	//does this system have localstorage?
	if (has_local_storage() ){ 
		//set up the config page
		set_up_config();
		//check the config
		check_config();
	} else { 
		hide_config();
		//switch to the player
		switch_to_page("#Player");
	}
	//pay attention to the backgrounding of this web app
	document.addEventListener("visibilitychange", visibilityChanged);
});

function increment_visibility_down() {
	visibility -= 1;
	if (reading_data) {
		reading_data = false;
		//stop polling
		stop_timer_get_download_status = true;
		if (has_event_source) {
			//stop listening to SSE
			disable_sse();
		} else {
			//stop poling
			stop_timer_get_player_status = true;
			
		}
	}
}

function increment_visibility_up() {
	visibility += 1;
	if (visibility>0) {
		visibility = 0;
	}
	if (visibility == 0) {
		reading_data = true;
		stop_timer_get_download_status = false;
		timer_get_download_status();
		//get the player status
		get_player_status();
		//start listening to SSE
		if (has_event_source) {
			//stop listening to SSE
			set_up_sse();
		} else {
			//start poling
			stop_timer_get_player_status = false;
			timer_get_player_status();
		}
	}
}

function visibilityChanged() {
	if ( document.hidden ) {
		increment_visibility_down();
	} else {
		increment_visibility_up();
	}
}

try {
 var stateRequest = new tizen.PowerStateRequest("DISPLAY", "DIM");
 tizen.power.request(stateRequest, onSuccess, onError, screen_changed);
} catch(error) {
	console.log('not tizen');
}

function check_config() {
	var url = "/system";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//process the video data
			check_config_confirmed();
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			console.log(type);
			check_config_failed();
		} 
	});
}

function check_config_confirmed() {
	//enable the config elements
		//disable the config elements
	$("#config_host").disable = false;
	$("#config_port").disable = false;
	button_enable("#button_config_update");
	//clear any config warning
	$("#config_warning").text("");
	//the config worked!
	//start running
	run();
	switch_to_page("#Player");
}

function check_config_failed() {
	//bummer dude
	var msg = "Failed to connected to "+host+":"+port;
	$("#config_warning").text( msg );
	//enable the config button
	button_enable("#button_config_update");
	switch_to_page("#Config");
}

function run() {
	//TODO: use SSE for download status
	stop_timer_get_player_status = false;
	timer_get_download_status();
	
	//can we do SSE?
	if (window.EventSource) {
		has_event_source = true;
		//get the status once
		get_player_status();
		//handle the event source
		set_up_sse();
	} else {
		stop_timer_get_player_status = false;
		// bummer dude, use the polling
		timer_get_player_status();
	}
	//show the player
	switch_to_page("#Player");
}

function set_up_resizes() {
	//what should happen when there is a resize?
	$(window).resize(function() {
		if (current_page=="#Player") {
			set_up_centereds();
		}
	});
}

function switch_to_page( page, source ) {
	if (source != undefined ) {
		if (button_disabled( $(source) ) ) {
			return;
		}
	}
	check_downloads = false;
		
	//hide the current page
	if (current_page != undefined) {
		$(current_page).hide();
	}
	$(page).show();
	current_page = page;
	switch(page) {
		case "#Player" :
			set_up_centereds();
			check_file_info_for_scrolling();
			break;
			
		case "#Video" :
			if ( got_video == false ) {
				got_video = true;
				get_video_content();
			}
			
			break;
		case "#Audio" :
			if ( got_audio == false ) {
				got_audio = true;
				get_audio_artists();
			}
			break;
		case "#Casts" :
			if ( got_casts == false ) {
				got_casts = true;
				get_feeds();
				check_downloads = true;
			}
			break;
		case "#AvailableEpisodes" :
			check_downloads = true;
			break;
		case "#Downloads" :
			check_downloads = true;
			break;
		case "#Streams" :
			if ( got_streams == false ) {
				got_streams = true;
				get_streams();
			}
	}
}

function set_up_centereds() {
	var cs = $(".centered");
	for( var i=0 ; i < cs.length ; i++ ) {
		var item = $(cs[i]);
		var cs_children = item.children();
		var iw = 0;
		for (var j = 0 ; j < cs_children.length ; j++) {
			if ( $(cs_children[j]).css('display') != 'none' ) {
				iw += $(cs_children[j]).outerWidth();
			}
		} 
		
		var pw = item.parent().innerWidth();
		var diff = (pw - iw)/2;
		item.css('margin-left', diff);
	}
}

function disable_sse() {
	sse_source.removeEventListener("positionduration", sse_positionduration, false);
	sse_source.removeEventListener("volume", sse_volume,false);
	sse_source.removeEventListener("artist", sse_artist, false);
	sse_source.removeEventListener("title", sse_title, false);
	sse_source.removeEventListener("newfile", sse_newfile, false);
	sse_source.removeEventListener("playtypechange", sse_playtypechange, false);
	sse_source.removeEventListener("playerstate", sse_playerstate, false);
	sse_source.removeEventListener("audio_update", sse_audio_update, false);
	sse_source.removeEventListener("audiorandom", sse_audiorandom, false);
	sse_source.removeEventListener("subtitles", sse_subtitles, false);
	sse_source.removeEventListener("muted", sse_muted, false);
	sse_source.removeEventListener("unmuted", sse_unmuted, false);
}

/* make a bunch of named functions
 * for handing sse data because javascript is shit
*/
sse_positionduration =  function(e) {
		var bits = e.data.split(":");
		update_player_position(bits[0], bits[1]);
	};
	
sse_volume = function(e) {
		update_volume(e.data);
	};

sse_artist = function(e) {
		waiting_for_tags = false;
		$("#artist").text( e.data );
		$("#artist").show();
		$("#filename").hide();
		check_file_info_for_scrolling();
	};
	
sse_title = function(e) {
		waiting_for_tags = false;
		$("#title").text( ": "+e.data );
		$("#title").show();
		$("#filename").hide();
		check_file_info_for_scrolling();
	};
	
sse_newfile = function(e) {
		waiting_for_tags = true;
		$("#filename").text( cleaner_filename( e.data ) );
		//add a timeout for showing the filename
		setTimeout(showfilename, 100);
	};

sse_playtypechange = function(e) {
		if (e.data != "PLAY_TYPE_AUDIO") {
			//enable the audio play button
			button_enable(audio_play_button);
		}
	};	
	
sse_playerstate = function(e) {
		process_player_state( e.data );
	}
	
sse_audio_update = function(e) {
		switch(e.data) {
			case "start" :
				button_disable(audio_update_button);
			break;
			case "finish" :
				button_enable(audio_update_button);
			break;
		}
	};

sse_audiorandom = function(e) {
		var checked = (e.data=="true") ? true : false;
		update_audio_random_checkbox(checked);
	};

sse_subtitles = function(e) {
		var bits = e.data.split(":");
		process_subtitle_data(parseInt(bits[0]), parseInt(bits[1]));
	};
	
sse_muted = function() {
		process_mute(true);
	};
	
sse_unmuted = function() {
		process_mute(false);
	};
	
function set_up_sse() {
	//what is the source of our events?
	var url = "/events";
	url = target_url(url);
	sse_source = new EventSource(url);
	//handle position duration 
	sse_source.addEventListener("positionduration", sse_positionduration);
	//handle volume 
	sse_source.addEventListener("volume", sse_volume);
	sse_source.addEventListener("artist", sse_artist);
	sse_source.addEventListener("title", sse_title);
	sse_source.addEventListener("newfile", sse_newfile);
	sse_source.addEventListener("playtypechange", sse_playtypechange);
	sse_source.addEventListener("playerstate", sse_playerstate);
	//handle audio updating 
	sse_source.addEventListener("audio_update", sse_audio_update);
	//handle player random 
	sse_source.addEventListener("audiorandom", sse_audiorandom);
	//handle the subtitles
	sse_source.addEventListener("subtitles", sse_subtitles);	
	//handle the mute unmute
	sse_source.addEventListener("muted", sse_muted);
	sse_source.addEventListener("unmuted", sse_unmuted);
}

function showfilename() {
	if (waiting_for_tags) {
		$("#artist").hide();
		$("#title").hide();
		$("#filename").show();
	}
}

function check_file_info_for_scrolling() {
	if (file_info_should_scroll()) {
		scroll_file_info();
	} else {
		//reset the file_info
		file_info.css('left','0px');
	}
}

function file_info_should_scroll() {
	//what is the width of the #file_info element?
	var fi_width = file_info.width();
	//what is the width of the window?
	var win_width = $(window).width(); 
	return win_width < fi_width ;
}

function scroll_file_info(){
	if (!file_info_is_scrolling) {
		file_info_is_scrolling = true;
		var fi_width = file_info.width();
		//what is the width of the window?
		var win_width = $(window).width();
		var diff = -(fi_width - win_width);
		var speed = 5000
		if ( parseInt(file_info.css('left')) < 0 ) {
			//we should scroll right
			diff = 0;
		}
		file_info.animate( 
			{ left: diff+"px" }, 
			speed,
			function() {
				file_info_is_scrolling = false;
				check_file_info_for_scrolling();
			}
		);
	}
}

/* this should have been abstracted out to a class */
//make a new target url based on url, host, and port
function target_url( path ) {
	var url = "http://"+host+":"+port+path;
	return url;
}
	

function get_video_content(path) {
	//do some ajaxy poo to get the episodes
	var url = (path) ? "/video/content/"+encodeURIComponent(path) : "/video/content";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//process the video data
			process_video_data(body);
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_video_content: "+type);
		} 
	});
}

function get_audio_artists() {
	last_audio_view_function = "audio_artists";
	current_selected_audio=undefined;
	var url = "/audio/artists";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//we should have some info about artists
			process_artist_data(body);
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_audio_artists: "+type);
		} 
	});
}

function get_player_status() {
	var url = "/player/status";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			var info_string = "";
			if( body['title']!="") {
				$("#title").text(": "+ body['title'] );
				$("#title").show();
				$("#filename").hide();
				if (body['artist']!="") {
					$("#artist").text( body['artist'] );
					$("#artist").show();
				}
			} else {
				$("#filename").text( cleaner_filename( body['filename'] ) );
				$("#filename").show();
				$("#artist").hide();
				$("#title").hide();
			}
			//do we need to scroll?
			check_file_info_for_scrolling();
			//handle the subtitle stuff
			process_subtitle_data( body["subtitle-count"], body["current-subtitle"] );
			//handle mute
			process_mute( body['muted'] );
			update_volume( body['volume'] );
			update_player_position(body['position'], body['duration']);
			//update the random
			update_audio_random_checkbox(body["audio-play-random"]);
			process_player_state( body['state'] );
			//TODO: move the play-type and random logic to their own functino
			//handle the play-type
			switch(body['play-type']) {
				case "PLAY_TYPE_AUDIO":
					
					break;
				default:
					button_enable(audio_play_button);
			}
			
			//TODO: this should be in a muttonchop status function
			if (!body['audio-updating']) {
				if ( button_disabled(audio_update_button) && last_audio_view_function =="audio_artists" ) {
					get_audio_artists();
				}
				button_enable(audio_update_button);
				$("#audio_status").text("");
			} else {
				button_disable(audio_update_button);
				$("#audio_status").text("Updating audio database: "+body['last-audio-file-path']);
			}
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_player_status: "+type);
		} 
	});
}

function process_artist_data(data) {
	//clear the title
	$("#audio_title").empty();
	//where do we need to attach the artist data?
	var al = $("#audio_list");
	al.empty();
	var artist_count = data.length;
	for (var i = 0 ; i < artist_count ; i++ ) {
		var alia = $('<div class="faux_button">');
		var artist_name = not_empty_value(data[i],"UNKNOWN");
		//handle clicks on the list_item
		alia.click( click_artist );
		alia.attr('name',artist_name);
		alia.html(artist_name);
		al.append(alia);		
	}
}

function process_streams_data(data) {
	$("#streams_list").empty();
	var streams = data['streams'];
	var streams_count = streams.length;
	for (var i = 0 ; i < streams_count ; i++ ) {
		var stream = streams[i];
		var stream_text = "";
		var sli = $("<div class='faux_button'>");
		if ( not_empty_value(stream['title']) ) {
			stream_text = stream['title'];
		} else {
			stream_text = stream['source'];
		}
		//handle clicks on the list_item
		sli.click( click_stream );
		sli.attr('stream_id',stream['id']);
		sli.html(stream_text);
		$("#streams_list").append(sli);		
	}
}

function process_artist_tracks(data) {
	//clear the view
	var al = $("#audio_list");
	al.empty();
	//set the header 
	var alt = $('<div class="faux_button_divider">');
	alt.html(data['artist']);
	al.append(alt);
	var track_count = data.files.length;
	for (var i = 0 ; i < track_count ; i++ ) {
		file = data.files[i];
		var alia = $('<div class="faux_button file_button">');
		//handle clicks on the list_item
		alia.click( click_audio_track );
		alia.attr('track_id',file['id']);
		//what text should we show?
		//what text should we show?
		var album = not_empty_value( file['album'], "Unknown");
		var title = not_empty_value( file['title'], "Unknown");
		var track_text = album + " / " + title;
		if( album == "Unknown" && title == "Unknown" ) {
			track_text = file['path'];
		}
		alia.html(track_text);
		al.append(alia);		
	}
}

function process_download_data(data) {
	var len = data['downloads'].length;
	var good_ids = new Array();
	if(len > 0 ) {			
		for(var i=0; i<len; i++) {
			dl = data['downloads'][i];
			var id = dl["id"];
			good_ids.push ( id );
			update_download( dl );
		}
	}
	//remove downloads that don't have a status
	$(".download_info").each( function() {
		var click_id =	$(this).attr('click_id');
		if($.inArray(click_id, good_ids)==-1 ){
			//get rid of this div
			$(this).remove();
			//mark as downloaded
			mark_downloaded( click_id );
			//do we need to show the play episode button?
			if (current_selected_episode!=undefined){ 
				if (click_id == $(current_selected_episode).attr("click_id")) {
					button_enable("#play_episode_button");
				}
			}
		}
	});
}


function process_audio_search_data(data) {
	//clear the audio list
	var al = $("#audio_list");
	al.empty();
	//add the search string as a divider
	al.append('<div class="faux_button_divider">' +data.message+ '</div>');
	var track_count = data.files.length;
	for (var i = 0 ; i < track_count ; i++ ) {
		file = data.files[i];
		var track = $('<div class="faux_button">');

		//handle clicks on the list_item
		track.click( click_audio_track );
		track.attr('track_id',file['id']);
		//what text should we show?
		var track_text = file['artist']+" : "+file['album']+" / "+file['title'];
		track.html(track_text);
		al.append(track);		
	}
}

function process_subtitle_data(count, current_selected) {
	ignore_subtitle_select_change = true;
	if (current_selected==undefined) {
		current_selected = 0;
	}
	if (count==0 || count==undefined) {
		//hide the subtitle control block
		$("#subtitle_control_block").hide();
	} else {
		$("#subtitle_select").empty();
		//clear the subtitle select
		
		//add the subtitle options
		selected = (current_selected==-1) ? "selected" : "";
		$("#subtitle_select").append( $("<option value='-1' "+selected+">None</option>") );
		// make sure the current_selected is selected
		for (var i=0; i < count; i++) {
			//add the subtitle options
			selected = (current_selected==i) ? "selected" : "";
			$("#subtitle_select").append( $("<option value='"+i+"' "+selected+">"+i+"</option>") );
		}
		//show the subtitle control block
		$("#subtitle_control_block").show();
	}
	ignore_subtitle_select_change = false;
}

function process_video_data(data) {
	//disable the play button
	var vl = $("#video_list");
	//clear the current_selected_video
	current_selected_video = undefined;
	//clear out the video_list
	vl.empty();
	//clear out the breadcrumbs
	$("#video_breadcrumbs").empty();
	var dir_separator = data['dir-separator'];
	var path = data['path'];
	//if there is a path, separate out the base dir
	var path_parts = rsplit(path, dir_separator);
	var base_dir_path = path_parts[0];
	var base_dir = (path_parts[1])?path_parts[1]:path;
	var dirs = path.split(dir_separator);
	//make a video refresh button
	var video_button = $('<div class="faux_button">');		 
	video_button.attr('path','');
	//handle clicks on the list_item
	video_button.click( click_video_directory );
	video_button.html( 'Video' );
	$("#video_breadcrumbs").append(video_button);
	// do we have path parts?
	if (dirs.length > 0 && dirs != "" ) {
		for ( var i=0; i< dirs.length; i++ ){
			var crumb_path = dirs.slice(0,i+1).join(dir_separator);
			var title = dirs[i];
			var crumb_button = $('<div class="faux_button">');		 
			crumb_button.attr('path',crumb_path);
			//handle clicks on the list_item
			crumb_button.click( click_video_directory );
			crumb_button.html( title );
			$("#video_breadcrumbs").append(crumb_button);
			
		}
	}

	//reset the content top to header bottom, just in case
	$("#video_content").css('top', $("#video_header").height());
	$("#video_content").scrollTop(0);

	//handle the directories
	var d = data['directories'];
	var dlen = d.length;
	if(d.length > 0 || path) {
		d.sort();
		//vl.append('<div class="faux_button_divider directory_divider"></div>');
	
	/*	
		//if there is a path, we need a .. link
		if (path) { 
			var vldia = $('<div class="faux_button directory_button">');		 
			vldia.attr('path', base_dir_path);
			//handle clicks on the list_item
			vldia.click( click_video_directory );
			var title = d[i];
			vldia.html( '..' );
			vl.append(vldia);
		}
		*/
		var dir_path = (path)? path+dir_separator:"";
		//loop through the directories
		for (var i = 0; i < dlen ; i++) {
			var vldia = $("<div class='faux_button directory_button'>");		 
			vldia.attr('path', dir_path+d[i]);
			//handle clicks on the list_item
			vldia.click( click_video_directory );
			var title = d[i];
			vldia.html( "&raquo; "+cleaner_filename( title ) );
			vl.append(vldia);
		}
	}
	//handle the files
	var files = data['files'];
	var flen = files.length;
	var file_path="";
	//is there a directory path?
	if(path) {
		file_path+=path+dir_separator;
	}
	if(files.length > 0) {
		//sort
		files.sort();
		//vl.append('<div class="faux_button_divider">Files</div>');
		//loop through the files
		for (var i = 0; i < flen ; i++) {
			var vlfia = $("<div class='faux_button file_button'>"); 
			vlfia.attr('file_path', file_path+files[i]);
			//add the "cleaner" name as the divs text
			vlfia.html( cleaner_filename(files[i]));
			vlfia.click( click_video_file );
			vl.append(vlfia);
		}
	}
}

function click_video_directory() {
	var path = $(this).attr('path');
	button_select(this);
	get_video_content(path);
}

function click_config_update() {
	//disable the config elements
	$("#config_host").disable = true;
	$("#config_port").disable = true;
	button_disable("#button_config_update");
	//get data from the form
	host = $("#config_host").val();
	port = $("#config_port").val();
	//record the data in the local store
	localStorage.setItem("host",host);
	localStorage.setItem("port",port);
	check_config();
}

function click_video_file() {
	if (current_selected_video) {
		//unselect
		button_deselect(current_selected_video);
	}
	current_selected_video = this;
	button_select(current_selected_video);
	//enable the playbutton
	button_enable(video_play_button);
}

function click_audio_track() {
	if (current_selected_audio) {
		button_deselect(current_selected_audio);
	}
	current_selected_audio = this;
	button_select(current_selected_audio);
	//enable the playbutton
	button_enable(audio_play_button);			
}

function click_stream() {
	if (current_selected_stream) {
		button_deselect(current_selected_stream);
	}
	current_selected_stream = this;
	button_select( this );
	//enable the playbutton
	button_enable(stream_play_button);			
	button_enable(stream_edit_button);			
	button_enable(stream_delete_button);			
}

function click_audio_play_button() {
	if ( button_disabled("#audio_play_button") ) {
		return;
	}
	var track_id;
	var url = "/audio/play";
	url = target_url(url);
	if(current_selected_audio){
		track_id = $(current_selected_audio).attr('track_id');
		var url = "/audio/play/"+track_id;
	url = target_url(url);
	}
	
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { 
			//disable the audio play button
			button_disable(audio_play_button);
		}, 
		error: function(xhr, type) { 
			log("error :click_audio_play_button: "+type);
		} 
	});
}

function disable_audio_search() {
	audio_search_button.attr('disabled',true);
	audio_search_text.attr('disabled',true);
}

function enable_audio_search() {
	audio_search_button.attr('disabled',false);
	audio_search_text.attr('disabled',false);
}

function click_audio_search_button() {
	last_audio_view_function = "audio_search";
	//disable the search components
	var search_text = prompt("Search:", "");
	if (search_text == null ) {
		return;
	} else {
		search_text = search_text.trim();
		if (search_text == "") {
			return;
		}
	}
	var text = encodeURIComponent(search_text);
	var url = "/audio/search/"+text;
	url = target_url(url);
	
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { 
			process_audio_search_data( body );
		}, 
		error: function(xhr, type) { 
			log("error :click_audio_search_button: "+type);
		} 
	});
}

function click_audio_update_button() {
	//disable the audio play button
	button_disable(audio_update_button);
	var url = "/audio/update";
	url = target_url(url);
	//this could take a while, confirm with the user
	var result = confirm("This could take a while. Continue?");
	if( !result ) {
		return;
	}
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { 
			
		}, 
		error: function(xhr, type) { 
			log("error :click_audio_update_button: "+type);
			audio_update_button.attr("disabled",false);
		} 
	});
}

function click_audio_random_checkbox() {
	var checked = audio_random_checkbox.is(":checked");
	var url = "/audio/random/"+checked;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { 
			//disable the audio play button
			button_disable(audio_play_button);
		}, 
		error: function(xhr, type) { 
			log("error :click_audio_random_checkbox: "+type);
		} 
	});
}

function click_video_play_button() {
	//what is the current
	var p = value_or_empty_string($(current_selected_video).attr('file_path'));
	//urlencode 
	p = encodeURIComponent(p);
	//disable the play button
	button_disable(video_play_button);
	var url = "/video/play/"+p;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anytdirpathing?
		}, 
		error: function(xdirpathr, type) { // type is a string ('error' for dirpathTTP errors, 'parsererror' for invalid JSON)
			log("error :click_video_play_button: "+type);
		} 
	});
}

function click_play() {
	if (button_disabled("#button_play") ){
		return;
	}
	var url = "/player/play";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_play: "+type);
		} 
	});
}
function click_pause() {
	var url = "/player/pause";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_pause: "+type);
		} 
	});
}

function click_stop() {
	var url = "/player/stop";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_stop: "+type);
		} 
	});
}

function click_next() {
	var url = "/player/next";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_next: "+type);
		} 
	});
}
function click_previous() {
	var url = "/player/previous";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_previous: "+type);
		} 
	});
}

function click_forward() {
	var url = "/player/forward";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_forward: "+type);
		} 
	});
}

function click_reverse() {
	var url = "/player/reverse";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_reverse: "+type);
		} 
	});
}

function click_volume_down() {
	volume_change(-2);
}

function click_volume_up() {
	 volume_change(2);
}
function click_volume_down_more() {
	volume_change(-6);
}

function click_volume_up_more() {
	 volume_change(6);
}

function click_mute() {
  var url = "/player/mute";
  url = target_url(url);
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anything?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_mute: "+type);
    } 
  });
}

function click_unmute() {
  var url = "/player/unmute";
  url = target_url(url);
  $.ajax({
    url: url,
    dataType: 'text', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
      //do we need to do anything?
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :click_unmute: "+type);
    } 
  });
}

function volume_change(num) {
	var url = "/player/volume_change/"+num;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do we need to do anything?
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :volume_change: "+type);
		} 
	});
}


/* stream click handlers */
function click_stream_play_button() {
	var id = $(current_selected_stream).attr("stream_id");
	var url = "/stream/play/"+id;
	url = target_url(url);
	button_disable(stream_play_button);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :play stream: "+type);
		} 
	});
}

function click_stream_delete_button() {
	var r = confirm("Really delete?");
	if (r == true) {
		var id = $(current_selected_stream).attr("stream_id");
		var url = "/stream/delete/"+id;
	url = target_url(url);
		$.ajax({
			url: url,
			dataType: 'text', 
			success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
				//refresh the streams list
				get_streams();
			}, 
			error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
				log("error :delete stream: "+type);
			} 
		});
	} 
}

function click_stream_confirm_button() {
	//get some data
	var source = $("#stream_source").val().trim();
	var title = $("#stream_title").val().trim();
	if (source == "") {
		alert( "You must enter a URL" );
		return;
	}
	if (title == "") {
		alert( "You must enter a title" );
		return;
	}
	if (editing_stream) {
		var id = $(current_selected_stream).attr("stream_id");
		//form a URL
		var url = "/stream/edit/"+encodeURIComponent(id);
	url = target_url(url);
		url += "?title="+encodeURIComponent(title);
		url += "&source="+encodeURIComponent(source);
	} else {
		var url = "/stream/add/"+encodeURIComponent(source)+"?title="+encodeURIComponent(title);
	url = target_url(url);
	}
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//refresh the streams list
			get_streams();
			//hide stuff
			switch_to_page("#Streams");
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			alert(xhr.responseText);
		} 
	});
}

function click_stream_new_button() {
	//we are not editing a stream
	editing_stream = false;
	//clear the info on the addedit page
	$("#stream_source").val('');
	$("#stream_title").val('');
	$("#streamAddEdit .title").text("New Stream");
	$("#stream_confirm_button").html("Add");
	switch_to_page("#StreamAddEdit");
}

function click_stream_edit_button() {
	//we are editing
	editing_stream = true;
	//clear the info on the addedit page
	$("#stream_source").val('');
	$("#stream_title").val('');
	$("#streamAddEdit .title").text("New Stream");
	$("#stream_confirm_button").html("Update");
	

	//get the id of the current_selected_stream
	var id = $(current_selected_stream).attr("stream_id");
	url = "/stream/info/"+id;
		$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			$("#stream_title").val( body['title'] );
			$("#stream_source").val( body['source'] );
			switch_to_page("#StreamAddEdit");
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			alert("Failed to get data for stream "+id);
		} 
	});
}

function click_stream_cancel_button() {
	$("#stream_source").val("");
	$("#stream_title").val("");
	$("#stream_add_edit").hide();
	$("#stream_blocker").hide();
	stream_new_button.attr('disabled', false);
}

function get_streams() {
	var url = "/stream/list";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//process the json
			process_streams_data(body);
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_previous: "+type);
		} 
	});
	
}

function connect_buttons() {
	//TODO: move these functions to the respective "set_up"
	video_play_button = $("#video_play_button");
	video_play_button.click( click_video_play_button );
	audio_play_button = $("#audio_play_button");
	audio_play_button.click( click_audio_play_button );
	audio_update_button = $("#audio_update_button");
	audio_update_button.click( click_audio_update_button );
	audio_random_checkbox = $("#audio_random_checkbox");
	audio_random_checkbox.click( click_audio_random_checkbox );
	audio_search_button = $("#audio_search_button");
	audio_search_text = $("#audio_search_text");
	audio_search_text.keydown( function(e){
		var key = e.which;
		if (key == 13) {
			click_audio_search_button();
		}
	});
	audio_search_button.click( click_audio_search_button );
	//disable some buttons
	button_disable(audio_play_button);
	button_disable(audio_update_button);
	button_disable(video_play_button);
	
	$("#audio_refresh_button").click( get_audio_artists );
	
	//handle the page navigation
	$(".page_nav").click ( function() {
		var target = $(this).attr("target");
		switch_to_page(target, this);
	});
}

function hide_config() {
	//hide the config button
	$("#nav_config").hide();
	//do we need to hide the config page?
}

function set_up_config() {
	var data_host = localStorage.getItem("host");
	if (data_host != undefined) {
		host = data_host;
	}
	
	var data_port = localStorage.getItem("port");
	if (data_port != undefined) {
		port = data_port;
	}
	
	//set the config input values
	$("#config_host").val(host);
	$("#config_port").val(port);
	$("#button_config_update").bind("click", click_config_update);
	$("#config_host").keypress(function (e) {
		if (e.which == 13) {
			//consider this a update click
			click_config_update();
			return false; 
		}
	});
	
	$("#config_port").keypress(function (e) {
		if (e.which == 13) {
			//consider this a update click
			click_config_update();
			return false; 
		}
	});
}

function set_up_player() {
	//hide the pause button
	$("#button_pause").hide();
	//hide the unmute
	$("#button_unmute").hide();
	$("#range_volume").bind('change', change_volume );
	$("#button_play").bind('click', click_play );
	$("#button_pause").bind('click', click_pause );
	$("#button_stop").bind('click', click_stop );
	$("#button_previous").bind('click', click_previous );
	$("#button_next").bind('click', click_next );
	$("#button_forward").bind('click', click_forward );
	$("#button_reverse").bind('click', click_reverse );
	$("#button_volume_up").bind('click', click_volume_up );
	$("#button_volume_down").bind('click', click_volume_down );
	$("#button_volume_up_more").bind('click', click_volume_up_more );
	$("#button_volume_down_more").bind('click', click_volume_down_more );
	$("#button_mute").bind('click', click_mute );
	$("#button_unmute").bind('click', click_unmute );
	$("#subtitle_select").bind('change', change_subtitle_select);
}

function set_up_swipe() {
//TODO: remove if no swipe is implemented 
}

function click_artist() {
	current_selected_audio=undefined;
	var artist = $(this);
	var name = artist.text();
	$("#audio_title").html( name );
	var url = "/audio/artist_tracks/"+name;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_artist_tracks( body );
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_artist: "+type);
		} 
	});
}

function timer_get_player_status() {
	get_player_status();
	setTimeout("timer_get_player_status()", player_timer_speed);
}

function change_subtitle_select() {
	if (ignore_subtitle_select_change ) {
		return;
	}
	var val = $("#subtitle_select").val();
	var url = "/player/subtitle/"+val;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//what should we do?
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :change_subtitle_select: "+type);
		} 
	});
}

function change_volume(event) {
	if (!updating_volume) {
		var new_volume = $("#range_volume").val();
		if ( new_volume != current_volume ) {
			current_volume = new_volume;
			set_volume( new_volume );
		}
	}
}
function set_volume( volume ) {
	var url = "/player/volume/"+volume;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(vol) { // body is a string (or if dataType is 'json', a parsed JSON object)
				//nothing to do
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :click_volume: "+type);
		} 
	});
}

function update_player_position( position, duration) {
	var percent = 0;
	if ( duration ) {
		percent = (position/duration)*100;
		var text = cleaner_time(position) +" / "+cleaner_time(duration);
		$("#progress_bar").html( text );
		$("#progress_frame_text").html( text );
	}
	$("#progress_bar").width(percent+"%");
}

function update_volume( vol ) {
	updating_volume = true;
	if (current_volume != vol ) {
		$("#range_volume").val(vol);
		$("#volume_value").html(vol);
		current_volume = vol;
	}
	updating_volume = false;
}
	
function update_audio_random_checkbox( val ) {
	$("#audio_random_checkbox").attr("checked",val);
}

/* some cast specific stuff */

function set_up_casts(){	
	/* connect button clicks */
	$("#casts_add_feed_button").click( clicked_casts_add_feed_button );
	$("#casts_update_feeds_button").click( clicked_casts_update_feeds_button );
	$("#casts_update_feed_button").click( clicked_casts_update_feed_button );
	$("#casts_delete_feed_button").click( clicked_casts_delete_feed_button );
	$("#available_episodes_download_button").click(clicked_available_episodes_download_button);
	
	$("#play_episode_button").click( clicked_play_episode_button );
	
	$("#download_episode_button").click( clicked_download_episode_button );
	/* disable buttons */
	$("#delete_feed_button").attr('disabled', true);
	button_disable("#play_episode_button");
	button_disable("#download_episode_button");
	button_disable("#casts_update_feeds_button");
	/* hide what needs hiding */
}

function set_up_streams() {
	//set up the stream buttons
	$("#stream_play_button").click( click_stream_play_button );
	$("#stream_edit_button").click( click_stream_edit_button );
	$("#stream_delete_button").click( click_stream_delete_button );
	$("#stream_new_button").click( click_stream_new_button );
	$("#stream_cancel_button").click( click_stream_cancel_button );
	$("#stream_confirm_button").click( click_stream_confirm_button );
	//disable some buttons
	button_disable("#stream_edit_button");
	button_disable("#stream_delete_button");
	button_disable("#stream_play_button");
}

function timer_get_download_status() {
	get_download_status();
	setTimeout("timer_get_download_status()", download_status_speed);
}

function clicked_kill() {
	//get the id of the clicked kill
	var id = $(this).attr("click_id");
	//get rid of the download div
	$(".download").each( function() {
		var click_id = $(this).attr('click_id');
		if ( click_id == id) {
			//get rid of this div
			$(this).remove();
		}
	});
	//ajax query to kill the dl
	var url = "/catcher/kill/"+id;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do nothing
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_kill: "+type);
		} 
	});
}

function get_feeds() {
	var url = "/catcher/feeds";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_feed_data( body );
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_feeds: "+type);
		} 
	});
}

function mark_played( id ) {
	//what target do we need to change?
	var target_ref = "#episode_"+id+" .played";
	var element = $(target_ref);
	element.html("*");
}

function mark_downloaded( id ) {
	//what target do we need to change?
	var target = $("#episode_"+id+" .downloaded");
	target.html("*");
}

function get_download_status() {
	if (check_downloads == false) {
		return;
	}
	var url = "/catcher/download_status";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_download_data( body );
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_download_status: "+type);
		} 
	});
}

function get_episode(id) {
	var url = "/catcher/episode/"+id;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			if( body ) {			
				// determine if this episode exists locally
				if (body['downloaded'] && body['exists']) {
					//enable the play button
					button_enable("#play_episode_button");
				} 
				else {
					//check if the episode is currently being downloaded
					var did = "download_"+id;
					if ( $("#"+did).length ){
						//the episode is being downloaded

					}
					else{
						button_enable("#download_episode_button");
					}
				}
			}
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_episode: "+type);
		} 
	});
}

function get_episodes(id) {
	var url = "/catcher/episodes/"+id;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_episodes_data( body );
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :get_episodes: "+type);
		} 
	});
}
		
function clicked_casts_update_feeds_button() {
	button_disable("#casts_update_feeds_button");
	var url = "/catcher/update/all";
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_update_feed_data(body);
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_casts_update_feeds_button: "+type);
		} 
	});
}
function clicked_casts_update_feed_button() {
	button_disable("#casts_update_feed_button");
	var url = "/catcher/update/"+current_selected_feed_id;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			button_enable("#casts_update_feed_button");
			process_update_feed_data(body);
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_casts_update_feed_button: "+type);
		} 
	});
}

function clicked_casts_add_feed_button() {
	var url = prompt("Enter Feed URL","");
	if (url == null ) {
		return;
	} else {
		url = url.trim();
		if (url == "") {
			return;
		}
	}

	var url = "/catcher/add/"+encodeURIComponent( url );
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_add_feed_data(body);
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_submit_feed_button: "+type);
		} 
	});

}

function clicked_casts_delete_feed_button() {
	var r = confirm("Really delete "+current_selected_feed_title+"?");
	if (r == true) {
		var id = current_selected_feed_id;
		var url = "/catcher/delete/"+id;
	url = target_url(url);
		//do a jquery to remove the feed
		$.ajax({
			url: url,
			dataType: 'text', 
			success: function(body) {
				//we should reset the feeds list
				get_feeds();
				//switch to the casts page
				switch_to_page("#Casts");
			},
			error: function() {
				//what should be done on error?
			}
		});
	} 
}

function clicked_available_episodes_download_button() {
	var dls = new Array();	
	$(".available_episode_checkbox:checked").each( function(){
		var name = $(this).attr('name');
		dls.push( name );
	});
	if (dls.length == 0 ) {
		//nothing was selected
		return;
	}
	id_string = dls.join(":");
	//make a list of the available episodes checkboxes
	//collect the IDs of the checked boxes
	//send AJAXy data so shit can get downloaded
	var url = "/catcher/download/"+encodeURIComponent(id_string);
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//switch to the downloads page
			switch_to_page("#Downloads");
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_available_episodes_download_button: "+type);
		} 
	});
}

function clicked_available_episodes_cancel_button() {
	//what should we do? 
	switch_to_page("#Casts");
}

function clicked_play_episode_button() {
	//disable the button
	button_disable("#play_episode_button");
	var id = $(current_selected_episode).attr("click_id");
	playing_episode_id = id;
	var url = "/catcher/play/"+id;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//what should we do?
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_play_episode_button: "+type);
		} 
	});
}

function clicked_download_episode_button() {
	//disable the download button
	button_disable("#download_episode_button");
	var id = current_selected_episode_id;
	
	var url = "/catcher/download/"+id;
	url = target_url(url);
	$.ajax({
		url: url,
		dataType: 'text', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//what should we do?
			button_enable("#download_episode_button");
		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error :clicked_download_episode_button: "+type);
		} 
	});
}

function process_update_feed_data(data) { 
	button_enable("#casts_update_feeds_button"); 
	//are there episodes?
	var episodes = data['episodes'];
	if( episodes.length > 0) {
		$("#available_episodes_list").empty();
		for (var i=0; i< episodes.length; i++) {
			var	episode = episodes[i];
			var id = episode['id'];
			var title = episode['title'];
			var date = date_from_pubdate( episode['pubdate'] );
			var epi = $("<div class='faux_button'>");
			var epil = $("<label for='cb_"+id+"'>");
			var epild = $("<div>");
			var cb = $("<input type='checkbox' class='available_episode_checkbox' name='"+id+"' id='cb_"+id+"' value=1>");
			epild.append(cb);
			epil.append(epild);
			var t = $("<span class='episode_title'>");
			t.append( title );
			epild.append( t );
			var d = $("<span class='pub_date'>");
			d.append( date );
			epild.append( d );
			epi.append(epil);
			$("#available_episodes_list").append(epi);
		}
		switch_to_page("#AvailableEpisodes");
	} else {
		alert( "No New Episodes" );
	}
	if (data['error'] ) {
		alert( data['error'] );
	}
}

function process_player_state( state ) {
	//check a button to see if it is disabled
	if ( button_disabled("#button_play") ) {
			enable_player_buttons();
	}
	
	switch (state) {
		case "PLAYING" :
			enable_player_buttons();
			$("#button_play").hide();
			$("#button_pause").show();
	
			break;
		case "PAUSED" :
		case "READY" :
			$("#button_play").show();
			$("#button_pause").hide();
			break;
		default:
			disable_player_buttons();
			$("#button_pause").hide();
			break;
	}
}


function button_disable( button_id ){
	$(button_id).addClass("faux_button_disabled");
}

function button_enable( button_id ) {
	$(button_id).removeClass("faux_button_disabled");
	$(button_id).removeClass("file_button_disabled");
}

function button_disabled( button_id ) {
	return $(button_id).hasClass( "faux_button_disabled" );
}

function button_select( element ) {
	if ( $(element).hasClass('file_button') ){
		$(element).addClass("file_button_selected");
	} else {
		$(element).addClass("faux_button_selected");
	}
}

function button_deselect( element ) {
	if ( $(element).hasClass('file_button') ){
		$(element).removeClass("file_button_selected");
	} else {
		$(element).removeClass("faux_button_selected");
	}
}

function disable_player_buttons() {
	button_disable("#button_play");
	button_disable("#button_pause");
	button_disable("#button_stop");
	button_disable("#button_previous");
	button_disable("#button_next");
	button_disable("#button_reverse");
	button_disable("#button_forward");
}

function enable_player_buttons() {
	button_enable("#button_play");
	button_enable("#button_pause");
	button_enable("#button_stop");
	button_enable("#button_previous");
	button_enable("#button_next");
	button_enable("#button_reverse");
	button_enable("#button_forward");	
}

function process_mute( muted ) {
	if (muted) {
		//show unmute
		$("#button_unmute").show();
		//hide mute
		$("#button_mute").hide();
	} else {
		//show mute
		$("#button_mute").show();
		//hide unmute
		$("#button_unmute").hide();
	}
}

function process_add_feed_data(data) {
	if (data['episodes'].length > 0 ) {
		//the feeds were updated, show the new feeds
		get_feeds();
		process_update_feed_data(data);
	}else{
		alert("Could not process the RSS feed");
	}
}

function process_episodes_data( data ) {
	//clear any feeds that may exist
	$("#episodes_list").empty();
	len = data.length;
	if(len > 0 ) {			
		for(var i=0; i<len; i++) {
			var episode = data[i];
			//start making the episode object
			var id = episode['id'];
			var e = $("<tr class='faux_button file_button'>");
			e.attr("id", "episode_"+id);
			e.attr("click_id", id);
			var t = $("<td class='episode_title'>");
			t.html(episode['title']);
			e.append(t);
			
			var downloaded = $("<td class='episode_download_status status'>");
			if (episode['downloaded']==1) {
				downloaded.addClass("status_true");
			}
			downloaded.html( "&darr;" );
			e.append(downloaded);
	
			var played = $("<td class='episode_played_status status'>");
			if (episode['played']==1) {
				played.addClass("status_true");
			}
			played.html( "&rsaquo;" );
			e.append(played);
			var date = $("<td class='pub_date'>");
			date.html( date_from_pubdate(episode['pubdate'])	);
			e.append(date);
			//episode might get clicked
			e.click( click_episode );
			//append the info
			$("#episodes_list").append( e );
		}
	}
}

function process_feed_data( data ) {
	var len = data.length;
	if(len > 0 ) {	
		//clear any feeds that may exist
		$("#feeds_list").empty();
		button_enable("#casts_update_feeds_button");
		//create and add the 'unplayed'
		var unplayed = {'id':0, 'title':'Unplayed'}
		//connect the unplayed
		connect_feed( unplayed );
		for(var i=0; i<len; i++) {
			feed = data[i];
			connect_feed( feed );
		}
	}
}		

function connect_feed( feed ) {
	//start making the feed object
	var id = feed['id'];
	var fli = $("<div class='faux_button directory_button'>");
	fli.attr("id", "feed_"+id);
	fli.attr("click_id", id);
	//create the title 
	fli.html("&raquo; "+feed['title']);
	fli.click( click_feed );
	//append the feed
	$("#feeds_list").append(fli);
}

function click_feed(){
	current_selected_feed_id = $(this).attr("click_id");
	current_selected_feed_title = $(this).html();
	//set the title of the episodes 
	$("#feed_title").html(current_selected_feed_title);
	//get the episodes for this feed
	get_episodes( current_selected_feed_id );
	//switch to the casts episodes 'page'
	switch_to_page("#Episodes");
}	

function click_episode() {
	//have we switched buttons?
	if (current_selected_episode != this ) {
		current_selected_episode_id = $(this).attr("click_id");
		//highlight some div
		if (current_selected_episode!=undefined) {
			button_deselect( $(current_selected_episode) );
		}
		//get the episode data
		get_episode( current_selected_episode_id );
		current_selected_episode = this;
		button_select( $(current_selected_episode) );
	}
}
			
function date_from_pubdate( pubdate ) {
	var date_parts = pubdate.split(" ");
	return date_parts[0];
}

function update_download( dl ) {
	//what is the percent?
	var p = dl['percent'];
	if ( p > 100 ) {
		p=0;
	}
	var id = dl['id'];
	var did = "download_"+id;
	var didp = did+"_progress"
	var didpw = didp+"_wrapper"
	//does this exist?
	if( $("#"+did).length==0) {
		//create the download div
		var dl_div = $("<div class='download_info'>");
		dl_div.attr("id", did);

		dl_div.attr("click_id", id);
		var title = $("<div class='download_title'>");
		title.html( dl['feed-title']+": "+dl['title'] );
		var progw = $("<div class='progress_frame'>");
		var prog = $("<div id='"+didp+"' class='progress_bar'>");
		prog.width(p+'%');
		progw.append(prog);
		dl_div.append( title );
		dl_div.append( progw ); 
		var kill_dl_div = $("<button class='kill_download'>");
		kill_dl_div.html("kill");
		kill_dl_div.attr("click_id", id);
		kill_dl_div.click( clicked_kill );
		
		dl_div.append(kill_dl_div);
		
		
		$("#downloads_list").append( dl_div );
	}
	else{
		$("#"+didp).width(p+"%");
	}
}

//we should detect localstorage
function has_local_storage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch(e){
    return false;
  }
}

function log( string ) {
	html = $("#log").html();
	d = new Date();
	html+= d.toTimeString() +": "+ string + "\n";
	$("#log").html( html);
}
